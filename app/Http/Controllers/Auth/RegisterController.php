<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreUserRequest;
use App\Models\User;

class RegisterController extends Controller
{
    public function showRegistrationForm() {
        // Trả về giao diện trang đăng kí
        return view('client.auth.register');
    }

    public function register(StoreUserRequest $request) {
        // Lấy giá trị được truyền lên từ client
        $data = $request->except(['_token']);
        // Gọi hàm lưu dữ liệu
        if (User::store($data)) {
            // Nếu lưu được thì về trang chủ
            return redirect()->route('home')->with('success', 'Register successfully!');
        } else {
            // Nếu lưu thất bại thì quay lại trang đăng kí
            return redirect()->back()->with('error', 'Register failure!');
        }
    }
}
