<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Models\Permisson;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index($limit = 5)
    {
        // Lấy danh sách roles
        // Số lượng recỏrds 1 lần lấy phụ thuộc vào biến $limit (Nếu không truyền $limit vào thì lấy 5 records)
        $roles = Role::paginate($limit);
        // Đưa dữ liệu của $roles sang file view bằng hàm compact()
        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        // Lấy danh sách tất cả permission nhóm theo cột group
        // Danh sách này sẽ được dùng để hiển thị ở form tạo role
        $permissions = Permisson::all()->groupBy('group');
        // dd($permissions);
        // Đưa dữ liệu của $permissions sang file view bằng hàm compact()
        return view('admin.roles.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRoleRequest $request
     * @return RedirectResponse
     */
    public function store(CreateRoleRequest $request)
    {
        // Lưu roles
        $result = Role::storeHandle($request);
        if($result) {
            return redirect()->route('admin.roles.index')->with(['message' => 'Create new role suceessfully!']);
        } else {
            return redirect()->back()->with(['message' => 'Create new role failure!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View
     */
    public function edit($id)
    {
        // Lấy thông tin của role muốn chỉnh sửa theo id truyền lên (ở url)
        $role = Role::findOrFail($id);
        // Lấy danh sách tất cả permission nhóm theo cột group
        // Danh sách này sẽ được dùng để hiển thị ở form tạo role
        $permissions = Permisson::all()->groupBy('group');
        // Đưa dữ liệu của $permissions sang file view bằng hàm compact()
        return view('admin.roles.edit', compact('role', 'permissions'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRoleRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(UpdateRoleRequest $request, $id)
    {
        $result = Role::updateRole($request, $id);
        if($result) {
            return redirect()->route('admin.roles.index')->with(['message' => 'Update suceessfully!']);
        } else {
            return redirect()->back()->with(['message' => 'Update failure!']);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        if (Role::destroy($id)) {
            return redirect()->route('admin.roles.index')->with(['message' => 'Delete suceess']);
        } else {
            return redirect()->back()->with(['message' => 'Delete failure']);
        }
    }
}
