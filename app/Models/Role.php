<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role as ModelsRole;

class Role extends ModelsRole
{
    use HasFactory;

    protected $fillable=[
        'name',
        'display_name',
        'guard_name',
        'group'
    ];

    static function storeHandle($request) {
        $dataCreate = $request->except('_token');
        // dd($dataCreate);
        $dataCreate['guard_name'] = 'web';

        $role = self::create($dataCreate);

        // Nếu lưu thành công $role vào database
        if ($role) {
            $role->permissions()->attach($dataCreate['permission_ids']);
            return $role;
        } else {
            return false;
        }
    }

    static function updateRole($request, $id) {
        $dataUpdate = $request->all();
        $role = Role::findOrFail($id); // Retrieve the role instance by its ID
        if ($role->update($dataUpdate)){
            $role->permissions()->sync($dataUpdate['permission_ids']); // Sync the permissions
            return $role;
        } else {
            return false;
        }
    }


    static function getWithGroup(int $limit = 5) {
        return self::latest('id')->paginate($limit)->groupBy('group');
    }
}
