<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    protected $fillable=[
        'title',
        'author_id',
        'status',
        'content',
        'published_by',
        'published_at'
    ];

    public function details()
    {
        return $this->hasMany(ProductDetail::class);
    }

    // public function images()
    // {
    //     return $this->morphMany(Image::class,'imageable');
    // }

}
