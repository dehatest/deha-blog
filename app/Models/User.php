<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'gender',
        'date_of_birth',
        'phone',
        'is_admin'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    static function store($data) {
        try {
            // Tạo mới đối tượng User
            $user = new self; 
            // Mã hoá password ra chuỗi kí tự đặc biệt               
            $data['password'] = Hash::make($data['password']);
            // Lưu vào database
            if ($user->create($data)) {
                return true;
            } else {
                return false;
            }    
        } catch (\Exception $e) {
            // Nếu xảy ra bất cứ lỗi gì sẽ log lỗi ra file log
            Log::error($e);
            return [
                'success' => false,
                'message' => 'Something went wrong!'
            ];
        }
    }
    
    static function updateAdmin($request, $id) {
        try {
            $dataUpdate = $request->except('password');
            $user = self::findOrFail($id);
            if ($request->password) {
                $dataCreate['password'] = Hash::make($request->password);
            }
            
            // Lưu vào database
            if ($user->update($dataUpdate)) {
                $user->assignRoles($dataUpdate['role_ids'] ?? []);
                return [
                    'success' => true,
                    'message' => 'Update admin successfully!',
                    'data' => $user
                ];
            } else {
                return [
                    'success' => false,
                    'message' => 'Update admin failure!'
                ];
            }    
        } catch (\Exception $e) {
            // Nếu xảy ra bất cứ lỗi gì sẽ log lỗi ra file log
            Log::error($e);
        }
    }

    // static function findByEmailAndPassword($data) {
    //     try {
    //         $user = self::where('email', $data['email'])->where('password', $data['password'])->get();
    //         if (isset($user) && !empty($user)) {
    //             return $user;
    //         } else {
    //             return false;
    //         }
    //     } catch (\Exception $e) {
    //         Log::error($e);
    //     }
    // }


    /**
     * @param array|int $roles
     * @return array
     */
    public function assignRoles($roles)
    {
        return $this->roles()->sync($roles);
    }

    static function allAdmin() {
        return self::where('is_admin', 1)->get();
        // return self::where('is_admin', 1)->where('id', '<>', 1)->get();
    }

    static function storeAdmin($data) {
        try {
            // Tạo mới đối tượng User
            $user = new self;
            $data['is_admin'] = 1;
            // Mã hoá password ra chuỗi kí tự đặc biệt               
            $data['password'] = Hash::make($data['password']);
            // Save admin
            $user = $user->create($data);
            if ($user) {
                $user->assignRoles($data['role_ids'] ?? []);
                return [
                    'success' => true,
                    'message' => 'Create admin successfully!',
                    'data' => $user
                ];
            } else {
                return [
                    'success' => false,
                    'message' => 'Create admin failure!'
                ];
            }
            $user->assignRoles($data['role_ids'] ?? []);
        } catch (\Exception $e) {
            dd($e);
            return [
                'success' => false,
                'message' => 'Something went wrong!'
            ];
            // Nếu xảy ra bất cứ lỗi gì sẽ log lỗi ra file log
            Log::error($e);
        }
    }

}
