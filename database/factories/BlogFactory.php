<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BlogFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->name(),
            'author_id' => 0,
            'status' => 0,
            'content' => $this->faker->text(),
            'published_by' => 0,
            'published_at' => 0
        ];
    }
}
