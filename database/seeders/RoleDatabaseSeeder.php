<?php

namespace Database\Seeders;

use App\Models\Permisson;
use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class RoleDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $roles = [
                ['name' => 'super-admin', 'display_name' => 'Super Admin', 'group' => 'system'],
                ['name' => 'admin', 'display_name' => 'Admin', 'group' => 'system'],
                ['name' => 'employee', 'display_name' => 'Employee', 'group' => 'system'],
                ['name' => 'manager', 'display_name' => 'Manager', 'group' => 'system'],
                ['name' => 'user', 'display_name' => 'User', 'group' => 'system'],

        ];

        foreach($roles as $role){
            Role::updateOrCreate($role);
        }

        $superAdmin = User::whereEmail('admin@gmail.com')->first();

        if(!$superAdmin)
        {
            $superAdmin = User::factory()->create(['email' => 'admin@gmail.com']);
        }
        $superAdmin->assignRole('super-admin');


        $permissions = [
            ['name' => 'create-user', 'display_name' => 'Create user', 'group' => 'User'],
            ['name' => 'update-user', 'display_name' => 'Update user', 'group' => 'User'],
            ['name' => 'show-user', 'display_name' => 'Show user', 'group' => 'User'],
            ['name' => 'delete-user', 'display_name' => 'Delete user', 'group' => 'User'],

            ['name' => 'create-role', 'display_name' => 'Create Role', 'group' => 'Role'],
            ['name' => 'update-role', 'display_name' => 'Update Role', 'group' => 'Role'],
            ['name' => 'show-role', 'display_name' => 'Show Role', 'group' => 'Role'],
            ['name' => 'delete-role', 'display_name' => 'Delete Role', 'group' => 'Role'],

            ['name' => 'create-category', 'display_name' => 'Create category', 'group' => 'category'],
            ['name' => 'update-category', 'display_name' => 'Update category', 'group' => 'category'],
            ['name' => 'show-category', 'display_name' => 'Show category', 'group' => 'category'],
            ['name' => 'delete-category', 'display_name' => 'Delete category', 'group' => 'category'],

            ['name' => 'create-blog', 'display_name' => 'Create blog', 'group' => 'blog'],
            ['name' => 'update-blog', 'display_name' => 'Update blog', 'group' => 'blog'],
            ['name' => 'show-blog', 'display_name' => 'Show blog', 'group' => 'blog'],
            ['name' => 'delete-blog', 'display_name' => 'Delete blog', 'group' => 'blog']
    ];

    foreach($permissions as $item){
            Permisson::updateOrCreate($item);
        }
    }
}
