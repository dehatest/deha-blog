<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'thuyphucqn@gmail.com',
            'password' => Hash::make('thuyphuc'), // You should hash passwords
            'is_admin' => 1,
        ]);
    }
}
