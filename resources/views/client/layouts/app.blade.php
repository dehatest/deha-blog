    <!DOCTYPE html>

    <html lang="en-us">

    <head>
        <meta charset="utf-8">
        <title>Reporter - HTML Blog Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5">
        <meta name="description" content="This is meta description">
        <meta name="author" content="Themefisher">
        <link rel="shortcut icon" href="{{ asset('client') }}/images/favicon.png" type="image/x-icon">
        <link rel="icon" href="{{ asset('client') }}/images/favicon.png" type="image/x-icon">
    
        <!-- theme meta -->
        <meta name="theme-name" content="reporter" />

        <!-- # Google Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Neuton:wght@700&family=Work+Sans:wght@400;500;600;700&display=swap" rel="stylesheet">

        <!-- # CSS Plugins -->
        <link rel="stylesheet" href="{{ asset('client') }}/plugins/bootstrap/bootstrap.min.css">

        <!-- # Main Style Sheet -->
        <link rel="stylesheet" href="{{ asset('client') }}/css/style.css">
    </head>

    <body>

    <header class="navigation">
    <div class="container">
        @include('client.layouts.topbar')
    </div>
    </header>

    <main>
        @yield('content')
    </main>

    <footer class="bg-dark mt-5">
    <div class="container section">
        <div class="row">
        <div class="col-lg-10 mx-auto text-center">
            <a class="d-inline-block mb-4 pb-2" href="index.html">
            <img loading="prelaod" decoding="async" class="img-fluid" src="{{ asset('client') }}/images/logo-white.png" alt="Reporter Hugo">
            </a>
            <ul class="p-0 d-flex navbar-footer mb-0 list-unstyled">
            <li class="nav-item my-0"> <a class="nav-link" href="about.html">About</a></li>
            <li class="nav-item my-0"> <a class="nav-link" href="article.html">Elements</a></li>
            <li class="nav-item my-0"> <a class="nav-link" href="privacy-policy.html">Privacy Policy</a></li>
            <li class="nav-item my-0"> <a class="nav-link" href="terms-conditions.html">Terms Conditions</a></li>
            <li class="nav-item my-0"> <a class="nav-link" href="404.html">404 Page</a></li>
            </ul>
        </div>
        </div>
    </div>
    <div class="copyright bg-dark content">Designed &amp; Developed By <a href="https://themefisher.com/">Themefisher</a></div>
    </footer>


    <!-- # JS Plugins -->
    <script src="{{asset('client')}}/plugins/jquery/jquery.min.js"></script>
    <script src="{{asset('client')}}/plugins/bootstrap/bootstrap.min.js"></script>

    <!-- Main Script -->
    <script src="js/script.js"></script>

    </body>
    </html>
