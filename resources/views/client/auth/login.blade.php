@extends('client.layouts.app')
@section('title', 'Products')
@section('content')

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="row">
                    <div class="pr-0 pr-lg-12">
                        <div class="breadcrumbs mb-4"> <a href="{{ route('home') }}">Home</a>
                            <span class="mx-1">/</span>Login
                        </div>
                    </div>
                    <div class="pr-0 pr-lg-12">
                        <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labor.
                            <div class="mt-5">
                                <p class="h3 mb-3 font-weight-normal">You don't have an account?
                                </p>
                                <p class="mb-3"><a class="text-dark" href="{{ route('register') }}">Create a new account now!</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mt-4 mt-lg-0">
                <form method="POST" action="{{ route('login') }}" class="row">
                    {{-- Tránh lỗi CSRF (Yêu cầu form phải có của laravel với form) --}}
                    @csrf
                    {{-- END --}}
                    <div class="col-12">
                        <label for="email">Email</label>
                        <input type="text" class="form-control mb-4" placeholder="Enter your email" name="email" id="email" value="{{ old('email') }}" required autocomplete="off">
                        @error('name')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-12">
                        <label for="password">Password</label>
                        <input type="text" class="form-control mb-4" placeholder="Enter your password" name="password" id="password" required autocomplete="off">
                        @error('name')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-12 items-end">
                        <button class="btn btn-outline-primary" type="submit">Login</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
@endsection

