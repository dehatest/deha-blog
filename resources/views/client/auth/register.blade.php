@extends('client.layouts.app')
@section('title', 'Products')
@section('content')

<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="row">
                    <div class="pr-0 pr-lg-12">
                        <div class="breadcrumbs mb-4"> <a href="{{ route('home') }}">Home</a>
                            <span class="mx-1">/</span>Register
                        </div>
                    </div>
                    <div class="pr-0 pr-lg-12">
                        <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labor.
                            <div class="mt-5">
                                <p class="h3 mb-3 font-weight-normal">You have an account?
                                </p>
                                {{-- Chuyển đến trang login nếu muốn đăng nhập (Đã có acccount) --}}
                                <p class="mb-3"><a class="text-dark" href="{{ route('login') }}">Login now!</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mt-4 mt-lg-0">
                @isset($record)
                    
                @endisset
                @if (session('success'))
                    <div class="alert alert-success">{{ session('success') }}</div>
                @endif
                @if (session('error'))
                    <div class="text-danger">{{ session('error') }}</div>
                @endif
                <form method="POST" action="{{ route('register') }}" class="row">
                    @csrf
                    <div class="col-12">
                        <label for="email">Name</label>
                        <input type="text" class="form-control" placeholder="Enter your name" name="name" id="name" value="{{ old('name') }}" autocomplete="off">
                        @error('name')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-12">
                        <label for="email">Email</label>
                        <input type="text" class="form-control" placeholder="Enter your email" name="email" id="email" value="{{ old('email') }}" autocomplete="off">
                        @error('email')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-12">
                        <label for="password">Password</label>
                        <input type="text" class="form-control" placeholder="Enter your password" name="password" id="password" value="{{ old('password') }}" autocomplete="off">
                        @error('password')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-12">
                        <label for="confirm_password">Confirm Password</label>
                        <input type="text" class="form-control" placeholder="Confirm your password" name="confirm_password" id="confirm_password" value="{{ old('confirm_password') }}" autocomplete="off">
                        @error('confirm_password')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="col-12 items-end">
                        <button class="btn btn-outline-primary" type="submit">Register</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
@endsection

