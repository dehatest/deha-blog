@extends('admin.layouts.app')
@section('title', 'Blogs')
@section('content')
    <div class="card">

        <h1>
            Blog list
        </h1>
        @if (session('message'))
            <h1 class="text-primary">{{ session('message') }}</h1>
        @endif
        @can('create-blog')
        <div>
            <a href="{{ route('admin.blogs.create') }}" class="btn btn-primary">Create</a>
        </div>
        @endcan
        <div>
            <table class="table table-hover">
                <tr>
                    <th>#</th>
                    {{-- <th>Image</th> --}}
                    <th>Title</th>
                    <th>Content</th>
                    <th>Status</th>
                </tr>

                @foreach ($blogs as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        {{-- <td><img src="{{ $item->image_path }}" width="200px" height="200px" alt=""></td> --}}
                        <td>{{ $item-> title }}</td>
                        {{-- <td>{{ $item-> Author }}</td> --}}
                        <td>{{ $item-> content }}</td>
                        <td>{{ $item-> Status }}</td>
                        <td>
                            @can('update-user')
                                <a href="{{ route('admin.blogs.edit', $item->id) }}" class="btn btn-warning">Edit</a>
                            @endcan
                            @can('delete-user')
                                <form action="{{ route('admin.blogs.destroy', $item->id) }}" id="form-delete{{ $item->id }}"
                                    method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-delete btn-danger" type="submit"
                                        data-id={{ $item->id }}>Delete</button>
                                </form>
                            @endcan
                        </td>
                    </tr>
                @endforeach
            </table>
            {{-- {{ $users->links() }} --}}
        </div>

    </div>

@endsection

@section('script')
@endsection
