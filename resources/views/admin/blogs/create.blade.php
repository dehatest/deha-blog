@extends('admin.layouts.app')
@section('title', 'Create User')
@section('content')
    <div class="card">
        <h1>Create New Blog</h1>

        <div>
            
            <form action={{ route('admin.blogs.store') }} method="post" id="createform" enctype="multipart/form-data">
                @csrf
                {{-- <div class=" input-group-static col-5 mb-4">
                    <label>Image</label>
                    <input type="file" accept="image/*" name="image" id="image-input" class="form-control">

                    @error('image')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>
                 --}}
                <div class="input-group input-group-static mb-4">
                    <label> Title </label>
                    <input type="title" value="{{ old('title') }}" name="title" class="form-control">

                    @error('title')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>

                <div class="input-group input-group-static mb-4">
                    <label> Status </label>
                    <input type="text" value="{{ old('text') }}" name="text" class="form-control">
                    {{-- <select name="status" class="form-control">
                        <option value="public">Public</option>
                        <option value="private">Private</option>
                    </select>                   --}}
                    @error('status')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>
               
                <div class="input-group input-group-static mb-4">
                    <label> Content </label>
                    <input type="text" value="{{ old('text') }}" name="text" class="form-control">
                    
                    @error('text')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>

                <button type="submit" class="btn btn-submit btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection


@section('script')

    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
    <script>
        $(() => {
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#show-image').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#image-input").change(function() {
                readURL(this);
            });
        });
    </script>
@endsection
