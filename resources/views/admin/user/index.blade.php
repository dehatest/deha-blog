@extends('admin.layouts.app')
@section('title', 'Users')
@section('content')
    <div class="card">

        <h1>
            User list
        </h1>
        @if (session('message'))
            <div class="alert alert-success alert-dismissible text-white" role="alert">
                <span class="text-sm">{{ session('message') }}</span>
                <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
        @endif
        <div>
            <a href="{{ route('admin.users.create') }}" class="btn btn-primary">Create</a>
        </div>
        <div>
            <table class="table table-hover">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Action</th>
                </tr>

                @foreach ($users as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->phone }}</td>
                        <td class="d-flex" style="gap: 1rem">
                            {{-- @can('update-user') --}}
                                <a href="{{ route('admin.users.edit', $item->id) }}" class="btn btn-warning mr-1"><i class="material-icons opacity-10">edit</i></a>
                            {{-- @endcan --}}
                            {{-- @can('delete-user') --}}
                                <form action="{{ route('admin.users.destroy', $item->id) }}" id="form-delete{{ $item->id }}"
                                    method="post">
                                    @csrf
                                    @method('delete')
                                    <button class="btn btn-delete btn-danger" type="submit"
                                        data-id={{ $item->id }}><i class="material-icons opacity-10">delete</i></button>
                                </form>
                            {{-- @endcan --}}
                        </td>
                    </tr>
                @endforeach
            </table>
            {{-- {{ $users->links() }} --}}
        </div>

    </div>

@endsection

@section('script')
@endsection
