@extends('admin.layouts.app')
@section('title', 'Create User')
@section('content')
    <div class="card">
        <h3>Update User {{ $user->name }}</h1>
        @if (session('error'))
        <div class="alert alert-danger alert-dismissible text-white" role="alert">
            <span class="text-sm">{{ session('error') }}</span>
            <button type="button" class="btn-close text-lg py-3 opacity-10" data-bs-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
        </div>
        @endif
        <div>
            <form action="{{ route('admin.users.update', $user->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('put')

                <div class="input-group input-group-static mb-4">
                    <label>Name</label>
                    <input type="text" value="{{ old('name') ?? $user->name  }}" name="name" class="form-control">

                    @error('name')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Email</label>
                    <input type="email" value="{{ old('email') ?? $user->email }}" name="email" class="form-control" autocomplete="off">
                    @error('email')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Phone</label>
                    <input type="text" value="{{ old('phone') ?? $user->phone }}" name="phone" class="form-control">
                    @error('phone')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>

                <div class="input-group input-group-static mb-4">
                    <label class="ms-0">Gender</label>
                    <select name="gender" class="form-control">
                        <option value="male">Male</option>
                        <option value="female">FeMale</option>
                    </select>

                    @error('gender')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Date of birth</label>
                    <input type="date" name="date_of_birth" class="form-control" value="{{ old('date_of_birth') ?? $user->date_of_birth }}"> 
                    @error('date_of_birth')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Password</label>
                    <input type="password" name="password" class="form-control" autocomplete="off">
                    @error('password')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>

                <div class="input-group input-group-static mb-4">
                    <label>Confirm password</label>
                    <input type="password" name="confirm_password" class="form-control" autocomplete="off">
                    @error('confirm_password')
                        <span class="text-danger"> {{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="">Roles</label>
                    <div class="row">
                        @foreach ($roles as $groupName => $role)
                            <div class="col-5">
                                <h4>{{ $groupName }}</h4>
                                
                                <div>
                                    @foreach ($role as $item)
                                        @if($item->id == 1)
                                            @hasrole('super-admin')
                                            <div class="form-check">
                                                <input class="form-check-input" name="role_ids[]"
                                                    {{ $user->roles->contains('id', $item->id) ? 'checked' : '' }}
                                                    type="checkbox" value="{{ $item->id }}">
                                                <label class="custom-control-label"
                                                    for="customCheck1">{{ $item->display_name }}</label>
                                            </div>
                                            @endhasrole
                                        @else
                                            <div class="form-check">
                                                <input class="form-check-input" name="role_ids[]"
                                                    {{ $user->roles->contains('id', $item->id) ? 'checked' : '' }}
                                                    type="checkbox" value="{{ $item->id }}">
                                                <label class="custom-control-label"
                                                    for="customCheck1">{{ $item->display_name }}</label>
                                            </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>


                <button type="submit" class="btn btn-submit btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection


@section('script')

    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
    <script>
        $(() => {
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) {
                        $('#show-image').attr('src', e.target.result);
                    };
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#image-input").change(function() {
                readURL(this);
            });
        });
    </script>
@endsection
