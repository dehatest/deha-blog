@extends('admin.layouts.app')
@section('title', 'Roles')
@section('content')
    <div class="card">

        @if (session('message'))
            <h1 class="text-primary">{{ session('message') }}</h1>
        @endif


        <h1>
            Role list
        </h1>
        <div>
            <a href="{{ route('admin.roles.create') }}" class="btn btn-primary">Create</a>

        </div>
        <div>
            <table class="table table-hover">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>DisplayName</th>
                    <th>Action</th>
                </tr>

                @foreach ($roles as $role)
                    <tr>
                        <td>{{ $role->id }}</td>
                        <td>{{ $role->name }}</td>

                        <td>{{ $role->display_name }}</td>
                        <td class="d-flex" style="gap: 1rem">
                            <a href="{{ route('admin.roles.edit', $role->id) }}" class="btn btn-warning"><i class="material-icons opacity-10">edit</i></a>

                            <form action="{{ route('admin.roles.destroy', $role->id) }}" id="form-delete{{ $role->id }}"
                                method="post">
                                @csrf
                                @method('delete')

                                <button class="btn btn-delete btn-danger" data-id={{ $role->id }}><i class="material-icons opacity-10">delete</i></button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
            {{ $roles->links() }}
        </div>

    </div>

@endsection
