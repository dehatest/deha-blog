<?php

use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Auth\AdminLoginController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('client.home.index');
})->name('home');

Auth::routes();
Route::get('/admin/login', [AdminLoginController::class, 'showLoginForm'])->name('admin.showLoginForm');
// Trang đăng nhập của admin
Route::post('/admin/login', [AdminLoginController::class, 'loginAdmin'])->name('admin.loginAdmin');

// Nhóm routes cho trang admin  (/admin/...)
Route::prefix('/admin')->name('admin.')->middleware(['admin'])->group(function () {
    // Trang chủ của admin (localhost/admin/dashboard)
    Route::get('/dashboard', function () {
        return view('admin.dashboard.index');
    })->name('dashboard');

    
    // Example of protecting a route in routes/web.php
    // Route::middleware(['auth', 'admin'])->group(function () {
    //     Route::get('/admin/dashboard', [AdminController::class, 'dashboard'])->name('admin.dashboard');
    // });

    Route::resource('roles', RoleController::class);
    Route::resource('categories', CategoryController::class);
    Route::resource('blogs', BlogController::class);
    Route::resource('users', UserController::class);
});
